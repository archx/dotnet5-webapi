# Swagger 文档的使用

`Visual Studio `中直接启动服务，将自动打开浏览器窗口

![](../assets/c02_01.jpg)

**让接口文档显示注释信息**

1. 修改 `Startup.cs`

```C#
public void ConfigureServices(IServiceCollection services)
{

    services.AddControllers();
    services.AddSwaggerGen(c =>
    {
        c.SwaggerDoc("v1", new OpenApiInfo { Title = "WebIM.API", Version = "v1" });
        // 添加
        var basePath = Path.GetDirectoryName(typeof(Program).Assembly.Location);
        c.IncludeXmlComments(Path.Combine(basePath ?? "/", "WebIM.API.xml"));
        c.DocInclusionPredicate((docName, description) => true);
    });
}
```

2. 在接口中添加注释

```c#
/// <summary>
/// 获取天气预报信息
/// </summary>
/// <returns>天气预报信息</returns>
[HttpGet]
public IEnumerable<WeatherForecast> Get()
{
    var rng = new Random();
    return Enumerable.Range(1, 5).Select(index => new WeatherForecast
    {
        Date = DateTime.Now.AddDays(index),
        TemperatureC = rng.Next(-20, 55),
        Summary = Summaries[rng.Next(Summaries.Length)]
    })
    .ToArray();
}
```

3. 在 解决方案资源管理器 窗口中的 项目上右键选择 ”属性“

![](../assets/c02_02.jpg)

在”生成”菜单下勾选 `XML 文档文件(X)`

4. 重启服务，即可在新打开的浏览器窗口中看到 swagger 注释内容

![](../assets/c02_03.jpg)