# Quartz.NET 任务调度

> 安装 Quartz

```text
dotnet add package Quartz.AspNetCore
```

> 创建作业调度类

```c#
namespace WebIM.API.Schedules.Quartz
{
    /// <summary>
    /// 作业调度
    /// </summary>
    public class JobSchedule
    {
        /// <summary>
        /// 构造任务调度
        /// </summary>
        /// <param name="jobType">Job类型</param>
        /// <param name="cronExpression">cron表达式</param>
        public JobSchedule(Type jobType, string cronExpression)
        {
            JobType = jobType ?? throw new ArgumentNullException(nameof(jobType));
            CronExpression = cronExpression ?? throw new ArgumentNullException(nameof(cronExpression));
        }

        /// <summary>
        /// Job 类型
        /// </summary>
        public Type JobType { get; private set; }

        /// <summary>
        /// Cron 表达式
        /// </summary>
        public string CronExpression { get; private set; }

        /// <summary>
        /// Job 运行状态
        /// </summary>
        public JobStatus JobStatus { get; set; } = JobStatus.Init;
    }

    /// <summary>
    /// 任务状态
    /// </summary>
    public enum JobStatus : byte
    {
        /// <summary>
        /// 初始化
        /// </summary>
        [Description("初始化")] Init = 0,

        /// <summary>
        /// 运行中
        /// </summary>
        [Description("运行中")] Running = 1,

        /// <summary>
        /// 调度中
        /// </summary>
        [Description("调度中")] Scheduling = 2,

        /// <summary>
        /// 已停止
        /// </summary>
        [Description("已停止")] Stopped = 3,
    }
}
```

> 实现 `IHostedService` , 设置 Quartz 调度程序

```c#
namespace WebIM.API.Schedules.Quartz
{
    /// <summary>
    /// IHostedService 实现，设置 Quartz 调度程序
    /// </summary>
    public class QuartzHostedService : IHostedService
    {
        private readonly ISchedulerFactory _schedulerFactory;
        private readonly IJobFactory _jobFactory;
        private readonly IEnumerable<JobSchedule> _jobSchedules;

        /// <summary>
        /// 调度器
        /// </summary>
        public IScheduler Scheduler { get; set; }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="schedulerFactory">调度器工厂</param>
        /// <param name="jobFactory">任务工厂类</param>
        /// <param name="jobSchedules">具体作业</param>
        public QuartzHostedService(ISchedulerFactory schedulerFactory, IJobFactory jobFactory,
            IEnumerable<JobSchedule> jobSchedules)
        {
            _schedulerFactory = schedulerFactory;
            _jobFactory = jobFactory;
            _jobSchedules = jobSchedules;
        }

        /// <summary>
        /// 应用程序启动时调用，循环注入作业计划，开始任务调度
        /// </summary>
        /// <param name="cancellationToken">token</param>
        /// <returns></returns>
        public async Task StartAsync(CancellationToken cancellationToken)
        {
            Scheduler = await _schedulerFactory.GetScheduler(cancellationToken);
            Scheduler.JobFactory = _jobFactory;
            foreach (var jobSchedule in _jobSchedules)
            {
                var job = CreateJob(jobSchedule);
                var trigger = CreateTrigger(jobSchedule);
                await Scheduler.ScheduleJob(job, trigger, cancellationToken);
                jobSchedule.JobStatus = JobStatus.Scheduling;
            }

            await Scheduler.Start(cancellationToken);
            foreach (var jobSchedule in _jobSchedules)
            {
                jobSchedule.JobStatus = JobStatus.Running;
            }
        }

        /// <summary>
        /// 应用程序结束时，结束任务调度
        /// </summary>
        /// <param name="cancellationToken">token</param>
        /// <returns></returns>
        public async Task StopAsync(CancellationToken cancellationToken)
        {
            if (Scheduler != null)
            {
                await Scheduler.Shutdown(cancellationToken);
            }

            foreach (var jobSchedule in _jobSchedules)
            {
                jobSchedule.JobStatus = JobStatus.Stopped;
            }
        }

        private static IJobDetail CreateJob(JobSchedule schedule)
        {
            var jobType = schedule.JobType;
            return JobBuilder
                .Create(jobType)
                .WithIdentity(jobType.FullName ?? jobType.Name)
                .WithDescription(jobType.Name)
                .Build();
        }

        private static ITrigger CreateTrigger(JobSchedule schedule)
        {
            return TriggerBuilder
                .Create()
                .WithIdentity($"{schedule.JobType.FullName}.trigger")
                .WithCronSchedule(schedule.CronExpression)
                .WithDescription(schedule.CronExpression)
                .Build();
        }
    }
}
```

> 创建 Quartz 作业执行器

```c#
namespace WebIM.API.Schedules.Quartz
{
    /// <summary>
    /// Quartz 作业执行器
    /// </summary>
    public class QuartzJobRunner : IJob
    {
        private readonly IServiceProvider _serviceProvider;

        /// <summary>
        /// 构造作业执行器
        /// </summary>
        /// <param name="serviceProvider"></param>
        public QuartzJobRunner(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        /// <summary>
        /// 执行具体的作业计划
        /// </summary>
        /// <param name="context">context</param>
        /// <returns></returns>
        public async Task Execute(IJobExecutionContext context)
        {
            // Scope 作用域
            using (var scope = _serviceProvider.CreateScope())
            {
                var jobType = context.JobDetail.JobType;
                if (scope.ServiceProvider.GetRequiredService(jobType) is IJob job)
                {
                    await job.Execute(context);
                }
            }
        }
    }
}
```

> 实现 `IJobFactory`, 创建 `QuartzJobRunner` 使用中间模式执行作业调度

```c#
namespace WebIM.API.Schedules.Quartz
{
    /// <summary>
    /// 中介者工厂实现
    /// </summary>
    public class RunnerJobFactory : IJobFactory
    {
        private readonly IServiceProvider _serviceProvider;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="serviceProvider">sp</param>
        public RunnerJobFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        /// <summary>
        /// 始终返回 QuartzJobRunner 实例，使用中介者模式执行
        /// </summary>
        /// <param name="bundle"></param>
        /// <param name="scheduler"></param>
        /// <returns></returns>
        public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler)
        {
            return _serviceProvider.GetRequiredService<QuartzJobRunner>();
        }

        /// <summary>
        /// ignore
        /// </summary>
        /// <param name="job"></param>
        public void ReturnJob(IJob job)
        {
            // throw new System.NotImplementedException();
        }
    }
}
```

> 配置Quartz作业调度

`Startup.cs` 中 `ConfigureServices`

```c#
#region quartz
// 添加 Quartz 服务
services.AddSingleton<IJobFactory, RunnerJobFactory>();
services.AddSingleton<ISchedulerFactory, StdSchedulerFactory>();
// 添加 Job
services.AddScoped<HelloWorldJob>();
services.AddSingleton(
    new JobSchedule(jobType: typeof(HelloWorldJob), cronExpression: "0/5 * * * * ?")
);
services.AddSingleton<QuartzJobRunner>();
services.AddHostedService<QuartzHostedService>();
#endregion
```

