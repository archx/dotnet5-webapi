# 使用log4net记录日志

## 添加依赖

> 使用 NuGet 添加依赖

- log4net
- Microsoft.Extensions.Logging.Log4Net.AspNetCore

![](../assets/c03_01.jpg)

## 使用配置

> 添加配置文件

新建 `Config` 文件夹，在文件夹内添加配置文件 `log4net.config`

注意修改配置文件属性为 `始终复制`

![](../assets/c03_02.jpg)

*配置文件内容*

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<log4net>
    <!-- 控制台日志配置 -->
    <appender name="Console" type="log4net.Appender.ConsoleAppender">
        <!-- 日志输出格式 -->
        <layout type="log4net.Layout.PatternLayout">
            <conversionPattern value="%5level [%thread] (%file:%line) - %message%newline" />
        </layout>
    </appender>

    <!-- 文件存储日志配置 -->
    <appender name="RollingFile" type="log4net.Appender.RollingFileAppender">
        <!-- 保存文件的名称 -->
        <file value="log\log.log" />
        <appendToFile value="true" />
        <lockingModel type="log4net.Appender.FileAppender+MinimalLock" />
        <!-- 文件的编码方式 -->
        <param name="Encoding" value="UTF-8"/>
        <!-- 每个文件的大小 KB|MB|GB-->
        <maximumFileSize value="3MB" />
        <datePattern value=".yyyyMMdd.log" />
        <!-- 保存文件数量 -->
        <maxSizeRollBackups value="10" />
        <!-- 日志输出格式 -->
        <layout type="log4net.Layout.PatternLayout">
            <conversionPattern value="%level %thread %logger - %message%newline" />
        </layout>
    </appender>

    <root>
        <level value="ALL" />
        <appender-ref ref="Console" />
        <appender-ref ref="RollingFile" />
    </root>
</log4net>
```

> 启用配置文件

修改 `Program.cs` 程序文件，添加 `Log4Net`

```c#
namespace WebIM.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args)
                .ConfigureLogging(builder =>
                {
                    builder.AddLog4Net("Config/log4net.config");
                })
                .Build().Run();
        }
    }
}
```

