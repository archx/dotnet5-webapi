﻿# Refit

> Refit 是一个受到 Square 的 Retrofit 库（Java）启发的自动类型安全 REST 库

Refit: The automatic type-safe REST library for .NET Core, Xamarin and .NET

[https://github.com/reactiveui/refit](https://github.com/reactiveui/refit)

参考

 - [.NET Core .NET 标准 REST 库 Refit](https://www.cnblogs.com/Leo_wl/p/12600094.html)
 - [.Net Core 下 HTTP 请求 IHttpClientFactory 示例详解](https://www.jb51.net/article/169922.htm)
 - [处理 ASP.NET Core Web API 中的错误](https://docs.microsoft.com/zh-cn/aspnet/core/web-api/handle-errors?view=aspnetcore-5.0)

## 集成 Refit

**安装依赖**

- Refit
- Refit.HttpClientFactory

**创建API接口**

```c#
namespace WebIM.API.Thirdparty.Easemob
{
    /// <summary>
    /// 环信服务端接口
    /// </summary>
    public interface IEasemobApi
    {
        /// <summary>
        /// 注册单个用户
        /// </summary>
        /// <param name="orgName">组织名称</param>
        /// <param name="appName">应用名称</param>
        /// <param name="input">注册信息</param>
        /// <returns></returns>
        [Post("/{org_name}/{app_name}/user")]
        Task<ApiResponse<RegisterUserOutput>> RegisterUser([AliasAs("org_name")] string orgName, 
            [AliasAs("app_name")] string appName,
            [Body] RegisterUserInput input);
    }
}
```

**授权信息处理**

创建 `AuthToken` 的管理接口

```c#
namespace WebIM.API.Thirdparty.Easemob
{
    /// <summary>
    /// 环信授权Token管理器接口
    /// </summary>
    public interface IEasemobAuthTokenStore
    {
        /// <summary>
        /// 获取Token
        /// </summary>
        /// <returns></returns>
        Task<string> GetTokenAsync();
    }
}
```

创建HttpClient请求处理器，用于请求时添加认证信息

```c#
namespace WebIM.API.Thirdparty.Easemob
{
    /// <summary>
    /// 环信授权处理器
    /// </summary>
    public class EasemobAuthHeaderHandler : DelegatingHandler
    {
        private readonly IEasemobAuthTokenStore _easemobAuthTokenStore;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="easemobAuthTokenStore">令牌管理器实现</param>
        public EasemobAuthHeaderHandler(IEasemobAuthTokenStore easemobAuthTokenStore)
        {
            _easemobAuthTokenStore = easemobAuthTokenStore;
        }

        /// <inheritdoc />
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
            CancellationToken cancellationToken)
        {
            var token = await _easemobAuthTokenStore.GetTokenAsync();
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);
            return await base.SendAsync(request, cancellationToken);
        }
    }
}
```

## 配置 Refit

在 `Startup.cs` 中启用 `HttpClient` 和 `Refit`

```c#
public void ConfigureServices(IServiceCollection services)
{
    #region Refit Client
    // 配置
    services.Configure<EasemobSettings>(Configuration.GetSection(nameof(EasemobSettings)));
    services.AddSingleton<IEasemobSettings>(sp =>
        sp.GetRequiredService<IOptions<EasemobSettings>>().Value);
    
    services.AddScoped<IEasemobAuthTokenStore, EasemobAuthTokenStoreService>();
    services.AddScoped(typeof(EasemobAuthHeaderHandler));
    
    // Refit Client
    services.AddRefitClient<IEasemobApi>()
        .ConfigureHttpClient(c => c.BaseAddress = new Uri("http://a1.easemob.com"))
        .AddHttpMessageHandler<EasemobAuthHeaderHandler>();
    #endregion
   
    // 启用 HttpClient
    services.AddHttpClient();
}
```

## 异常的处理

```c#
namespace WebIM.API.Controllers
{
    /// <summary>
    /// IM 用户信息接口
    /// </summary>
    [ApiController]
    [Route("/users")]
    public class ImUserController : ControllerBase
    {
        private readonly IEasemobSettings _eSettings;
        private readonly IEasemobApi _easemobApi;
        private readonly ILogger<ImUserController> _logger;

        /// <inheritdoc />
        public ImUserController(IEasemobSettings settings, IEasemobApi easemobApi, ILogger<ImUserController> logger)
        {
            _eSettings = settings;
            _easemobApi = easemobApi;
            _logger = logger;
        }

        /// <summary>
        /// 注册环信
        /// </summary>
        /// <param name="input">input</param>
        /// <returns>output</returns>
        [HttpPost]
        [Route("register")]
        [Produces("application/json")]
        public async Task<MessageCodeResult<RegisterUserOutput>> RegisterUser([FromBody] RegisterUserInput input)
        {
            var response = await _easemobApi.RegisterUser(_eSettings.OrgName, _eSettings.AppName, input);
            if (response.IsSuccessStatusCode)
            {
                return MessageCodeResult<RegisterUserOutput>.Ok(response.Content);
            }

            if (response.Error != null)
            {
                var desc = await response.Error.GetContentAsAsync<IDictionary<string, object>>();
                var message = EasemobErrorUtils.ResolveUserErrorMessage(desc);
                _logger.LogError("环信接口返回错误: {0}", message);
                return MessageCodeResult<RegisterUserOutput>.Fail(message);
            }

            return MessageCodeResult<RegisterUserOutput>.Fail("远程接口调用出错");
        }
    }
}
```


## 全局异常处理

> 处理自定义异常

```c#
namespace WebIM.API.Common.Exceptions
{
    /// <summary>
    /// 消息状态异常拦截器
    /// </summary>
    public class MessageCodeExceptionFilter : IActionFilter, IOrderedFilter
    {
        /// <inheritdoc />
        public void OnActionExecuting(ActionExecutingContext context)
        {
        }

        /// <inheritdoc />
        public void OnActionExecuted(ActionExecutedContext context)
        {
            if (context.Exception is MessageCodeException exception)
            {
                context.Result = new JsonResult(((IMessageCode) exception).ConvertTo());
                context.ExceptionHandled = true;
            }
        }

        /// <inheritdoc />
        public int Order { get; } = int.MaxValue - 10;
    }
}
```

在 `Startup.cs` 中添加

```c#
public void ConfigureServices(IServiceCollection services)
{
    services.AddControllers(options =>
        options.Filters.Add(new MessageCodeExceptionFilter()));
}
```

在 `Startup.cs` 使用中间件处理其他异常

```c#
public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
{
    // 默认异常处理器
    app.UseExceptionHandler(builder =>
    {
        builder.Run(async context =>
        {
            context.Response.StatusCode = 200;
            context.Response.ContentType = "application/json";

            var exception = context.Features.Get<IExceptionHandlerFeature>();
            if (exception is not null)
            {
                MessageCodeResult<object> resp = new MessageCodeResult<object>
                {
                    Code = 500, Message = exception.Error.Message
                };
                var output = JsonSerializer.Serialize(resp);
                await context.Response.WriteAsync(output).ConfigureAwait(false);
            }
        });
    });
}
```
