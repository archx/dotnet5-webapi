# 使用 MongoDB

参考

- [使用 ASP.NET Core 和 MongoDB 创建 Web API](https://docs.microsoft.com/zh-cn/aspnet/core/tutorials/first-mongo-app?view=aspnetcore-5.0&tabs=visual-studio)

## 添加依赖

- MongoDB.Driver

![](../assets/c04_01.jpg)

## 配置MongoDB

> 创建数据库

以 `docker` 为例; 进入容器创建数据库和账户

```shell
> docker exec -it 容器名称 mongo admin
> db.auth('admin','admin');
> use webim;
> db.createUser({user:"dotnet",pwd:"123456",roles:[{"role":"readWrite","db":"webim"}]});
```



![](../assets/c04_02.jpg)

> 加载配置文件内容

**创建 `MyMongoDatabaseSettings` 配置类**

```c#
namespace WebIM.API
{
    /// <summary>
    /// Mongdb 连接配置
    /// </summary>
    public class MyMongoDatabaseSettings : IMyMongoDatabaseSettings
    {
        /// <summary>
        /// 连接地址； eg: mongodb://dotnet:123456@127.0.0.1:27017/?authSource=webim
        /// </summary>
        public string ConnectionString { get; set; }
        /// <summary>
        /// 数据库名称
        /// </summary>
        public string DatabaseName { get; set; }
    }

    /// <summary>
    /// Mongdb 连接配置接口
    /// </summary>
    public interface IMyMongoDatabaseSettings
    {
        /// <summary>
        /// 连接字符串
        /// </summary>
        string ConnectionString { get; set; }

        /// <summary>
        /// 数据库名称
        /// </summary>
        string DatabaseName { get; set; }
    }
}
```

**在 `appsettings.json` 配置文件中添加同名配置**

```json
{
  "MyMongoDatabaseSettings": {
    "ConnectionString": "mongodb://dotnet:123456@127.0.0.1:27017/?authSource=webim",
    "DatabaseName": "webim"
  }
}
```

**在 `Startup.cs` 文件中读取配置**

```c#
public class Startup
{
    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
        // Mongodb
        services.Configure<MyMongoDatabaseSettings>(Configuration.GetSection(nameof(MyMongoDatabaseSettings)));
        services.AddSingleton<IMyMongoDatabaseSettings>(sp =>
            sp.GetRequiredService<IOptions<MyMongoDatabaseSettings>>().Value);
    }
}
```

