﻿# Autofac 依赖注入

**添加依赖**

- Autofac
- Autofac.Configuration
- Autofac.Extensions.DependencyInjection

## 使用 Autofac 替换自带的IoC容器

> 修改 `Program.cs` 主函数，添加 `AutofacServiceProviderFactory`

```c#
public static void Main(string[] args)
{
    CreateHostBuilder(args)
        .ConfigureLogging(builder =>
        {
            builder.AddLog4Net("Config/log4net.config");
        })
        .UseServiceProviderFactory(new AutofacServiceProviderFactory(builder =>
        {
            // 配置文件注册
            IConfigurationBuilder config = new ConfigurationBuilder();
            IConfigurationSource source = new JsonConfigurationSource()
            {
                Path = "Config/autofac.json",
                Optional = false,
                ReloadOnChange = true,
            };
            config.Add(source);
            ConfigurationModule module = new ConfigurationModule(config.Build());

            builder.RegisterModule(module);
        }))
        .Build().Run();
}
```

> 修改 `Startup.cs` 添加方法 `void ConfigureContainer(ContainerBuilder builder)`

```c#
/// <summary>
/// Autofac 自动调用该方法
/// </summary>
/// <param name="builder"></param>
public void ConfigureContainer(ContainerBuilder builder)
{
    // 此处注册服务
    builder.RegisterType(typeof(RequirePermissionAttribute))
        .PropertiesAutowired(new AutowiredPropertySelector());
}
```

## 配置文件

> `autofac.json`

```json
{
  "components": [
    {
      "type": "WebIM.API.Services.ImAccountService,WebIM.API",
      "services": [
        {
          "type": "WebIM.API.Services.IImAccountService,WebIM.API"
        }
      ],
      "instanceScope": "single-instance",
      "injectProperties": true
    }
  ]
}
```