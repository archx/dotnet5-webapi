# .NET5 WebAPI 学习使用记录

## ToDo List

- [x] 创建项目
- [x] [Swagger 文档的使用](./document/swagger.md)
- [x] [使用Log4Net记录日志](./document/log4net.md)
- [x] [使用 MongoDB](./document/mongodb.md)
- [x] [Quartz.NET 任务调度](./document/quartz.md)
- [x] [Autofac 依赖注入](./document/autofac.md)
- [x] [Refit 调用 REST API](./document/refit.md)
- [ ] 其他补充说明

## 创建项目

官方教程

- [教程：使用 ASP.NET Core 创建 Web API](https://docs.microsoft.com/zh-cn/aspnet/core/tutorials/first-web-api?view=aspnetcore-5.0&tabs=visual-studio-code)

**Visual Studio 2019**

点击 “文件” -> ”新建项目“，选择项目模板 `ASP.NET Core Web API`

![](./assets/c01_01.jpg)

点击”下一步 “，配置项目名称

![](./assets/c01_02.jpg)

点击”下一步 “，配置其他信息

![](./assets/c01_03.jpg)

**JetBrains Rider**

*File -> New Solution*

![](./assets/c01_r1.jpg)

