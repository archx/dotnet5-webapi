﻿using System.Threading.Tasks;

namespace WebIM.API.Thirdparty.Easemob
{
    /// <summary>
    /// 环信授权Token管理器接口
    /// </summary>
    public interface IEasemobAuthTokenStore
    {
        /// <summary>
        /// 获取Token
        /// </summary>
        /// <returns></returns>
        Task<string> GetTokenAsync();
    }
}