﻿using System.Collections.Generic;

namespace WebIM.API.Thirdparty.Easemob
{
    /// <summary>
    /// 环信接口错误消息解析
    /// </summary>
    public static class EasemobErrorUtils
    {
        
        /// <summary>
        /// 解析用户集成REST API 错误返回 <see href="https://docs-im.easemob.com/im/server/ready/user/" />
        /// </summary>
        /// <param name="errorContent">返回内容</param>
        /// <returns></returns>
        public static string ResolveUserErrorMessage(IDictionary<string, object> errorContent)
        {
            var error = "error";
            var errorDescription = "unknown";
            if (errorContent.ContainsKey("error"))
            {
                error = errorContent["error"] as string;
            }

            if (errorContent.ContainsKey("error_description"))
            {
                errorDescription = errorContent["error_description"] as string;
            }

            return $"{error}: {errorDescription}";
        }
    }
}