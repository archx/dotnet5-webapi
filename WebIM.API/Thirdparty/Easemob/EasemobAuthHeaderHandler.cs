﻿using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace WebIM.API.Thirdparty.Easemob
{
    /// <summary>
    /// 环信授权处理器
    /// </summary>
    public class EasemobAuthHeaderHandler : DelegatingHandler
    {
        private readonly IEasemobAuthTokenStore _easemobAuthTokenStore;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="easemobAuthTokenStore">令牌管理器实现</param>
        public EasemobAuthHeaderHandler(IEasemobAuthTokenStore easemobAuthTokenStore)
        {
            _easemobAuthTokenStore = easemobAuthTokenStore;
        }

        /// <inheritdoc />
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
            CancellationToken cancellationToken)
        {
            var token = await _easemobAuthTokenStore.GetTokenAsync();
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);
            return await base.SendAsync(request, cancellationToken);
        }
    }
}