﻿namespace WebIM.API.Thirdparty.Easemob
{
    /// <inheritdoc />
    public class EasemobSettings : IEasemobSettings
    {
        /// <inheritdoc />
        public string AppKey { get; set; }

        /// <inheritdoc />
        public string AppName { get; set; }

        /// <inheritdoc />
        public string OrgName { get; set; }

        /// <inheritdoc />
        public string ClientId { get; set; }

        /// <inheritdoc />
        public string ClientSecret { get; set; }
    }

    /// <summary>
    /// 环信设置
    /// </summary>
    public interface IEasemobSettings
    {
        /// <summary>
        /// 应用AppKey
        /// </summary>
        string AppKey { get; }

        /// <summary>
        /// 应用名称
        /// </summary>
        string AppName { get; }

        /// <summary>
        /// 组织名称
        /// </summary>
        string OrgName { get; }

        /// <summary>
        /// ClientID
        /// </summary>
        string ClientId { get; }

        /// <summary>
        /// ClientSecret
        /// </summary>
        string ClientSecret { get; }
    }
}