﻿using System.Text.Json.Serialization;

namespace WebIM.API.Thirdparty.Easemob.Models
{
    /// <summary>
    /// 错误响应
    /// </summary>
    public class BaseErrorResponse
    {
        /// <summary>
        /// 错误信息
        /// </summary>
        public string Error { get; set; }
        
        /// <summary>
        /// 错误描述
        /// </summary>
        [JsonPropertyName("error_description")]
        public string ErrorDescription { get; set; }

        /// <summary>
        /// 获取格式化过后的错误描述
        /// </summary>
        /// <returns>message</returns>
        public virtual string GetFormatMessage()
        {
            return $"{Error}: ${ErrorDescription}";
        }
    }
}