﻿using System.Text.Json.Serialization;

namespace WebIM.API.Thirdparty.Easemob.Models
{
    /// <summary>
    /// 访问令牌输出
    /// </summary>
    public class GetAccessTokenOutput
    {
        /// <summary>
        /// 访问令牌
        /// </summary>
        [JsonPropertyName("access_token")]
        public string AccessToken { get; set; }

        /// <summary>
        /// 过期时长：秒
        /// </summary>
        [JsonPropertyName("expires_in")]
        public int ExpiresIn { get; set; }

        /// <summary>
        /// 当前App的UUID
        /// </summary>
        public string Application { get; set; }
    }
}