﻿namespace WebIM.API.Thirdparty.Easemob.Models
{
    /// <summary>
    /// 用户实体
    /// </summary>
    public class UserEntity
    {
        /// <summary>
        /// UUID
        /// </summary>
        public string Uuid { get; set; }
        
        /// <summary>
        /// 类型
        /// </summary>
        public string Type { get; set; }
        
        /// <summary>
        /// 用户名
        /// </summary>
        public string Username { get; set; }
        
        /// <summary>
        /// 昵称
        /// </summary>
        public string Nickname { get; set; }
        
        /// <summary>
        /// 是否激活
        /// </summary>
        public bool Activated { get; set; }
        
        /// <summary>
        /// 创建时间戳
        /// </summary>
        public long Created { get; set; }
        
        /// <summary>
        /// 更新时间戳
        /// </summary>
        public long Modified { get; set; }
    }
}