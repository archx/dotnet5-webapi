﻿namespace WebIM.API.Thirdparty.Easemob.Models
{
    /// <summary>
    /// 注册用户输入
    /// </summary>
    public class RegisterUserInput
    {
        /// <summary>
        /// 用户名
        /// </summary>
        public string Username { get; set; }
        
        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }
    }
}