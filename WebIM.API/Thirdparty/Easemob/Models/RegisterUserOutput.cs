﻿using System.Collections.Generic;

namespace WebIM.API.Thirdparty.Easemob.Models
{
    /// <summary>
    /// 注册用户输出
    /// </summary>
    public class RegisterUserOutput
    {
        /// <summary>
        ///  action
        /// </summary>
        public string Action { get; set; }
        
        /// <summary>
        /// 应用ID
        /// </summary>
        public string Application { get; set; }

        /// <summary>
        /// 参数
        /// </summary>
        public object Params { get; set; }
        
        /// <summary>
        /// 请求路径
        /// </summary>
        public string Path { get; set; }
        
        /// <summary>
        /// 请求地址
        /// </summary>
        public string Uri { get; set; }

        /// <summary>
        /// 用户信息
        /// </summary>
        public IEnumerable<UserEntity> Entities { get; set; }

        /// <summary>
        /// unix 时间戳
        /// </summary>
        public long Timestamp { get; set; }
        
        /// <summary>
        /// 持续时间
        /// </summary>
        public int Duration { get; set; }
        
        /// <summary>
        /// 组织名称
        /// </summary>
        public string Organization { get; set; }
        
        /// <summary>
        /// 应用名称
        /// </summary>
        public string ApplicationName { get; set; }
    }
    
}