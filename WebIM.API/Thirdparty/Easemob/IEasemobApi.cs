﻿using System.Threading.Tasks;
using Refit;
using WebIM.API.Thirdparty.Easemob.Models;

namespace WebIM.API.Thirdparty.Easemob
{
    /// <summary>
    /// 环信服务端接口
    /// </summary>
    public interface IEasemobApi
    {
        /// <summary>
        /// 注册单个用户
        /// </summary>
        /// <param name="orgName">组织名称</param>
        /// <param name="appName">应用名称</param>
        /// <param name="input">注册信息</param>
        /// <returns></returns>
        [Post("/{org_name}/{app_name}/user")]
        Task<ApiResponse<RegisterUserOutput>> RegisterUser([AliasAs("org_name")] string orgName, 
            [AliasAs("app_name")] string appName,
            [Body] RegisterUserInput input);
    }
}