﻿using MongoDB.Bson.Serialization.Attributes;
using WebIM.API.Utilities;

namespace WebIM.API.Entities
{
    /// <summary>
    /// IM 通信账户
    /// </summary>
    public class ImAccount
    {
        /// <summary>
        /// Key
        /// </summary>
        [BsonId]
        public string AppKey { get; set; }

        /// <summary>
        /// 应用名称
        /// </summary>

        public string AppName { get; set; }

        /// <summary>
        /// 组织代码
        /// </summary>
        public string OrgName { get; set; }

        /// <summary>
        /// 客户端ID
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// 客服端密钥
        /// </summary>
        public string ClientSecret { get; set; }

        /// <summary>
        /// 访问令牌
        /// </summary>
        public string AccessToken { get; set; }

        /// <summary>
        /// Token 过期时间：秒
        /// </summary>
        public int ExpiresIn { get; set; } = 0;

        /// <summary>
        /// 过期时间戳
        /// </summary>
        public long ExpiredAt { get; set; } = 0;

        /// <summary>
        /// 当前应用uuid
        /// </summary>
        public string Application { get; set; }

        /// <summary>
        /// 创建时间戳
        /// </summary>
        public long CreatedAt { get; set; } = 0L;

        /// <summary>
        /// 更新时间戳
        /// </summary>
        public long UpdatedAt { get; set; } = 0L;
        
        /// <summary>
        /// 判断令牌是否过期
        /// </summary>
        /// <returns>bool</returns>
        public bool AccessTokenIsExpired()
        {
            return string.IsNullOrEmpty(AccessToken) || DateUtils.Timestamp() > ExpiredAt;
        }
    }
}