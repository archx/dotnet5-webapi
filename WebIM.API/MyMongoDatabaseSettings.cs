﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebIM.API
{
    /// <summary>
    /// Mongdb 连接配置
    /// </summary>
    public class MyMongoDatabaseSettings : IMyMongoDatabaseSettings
    {
        /// <summary>
        /// 连接地址； eg: mongodb://dotnet:123456@127.0.0.1:27017/?authSource=webim
        /// </summary>
        public string ConnectionString { get; set; }
        /// <summary>
        /// 数据库名称
        /// </summary>
        public string DatabaseName { get; set; }
    }

    /// <summary>
    /// Mongdb 连接配置接口
    /// </summary>
    public interface IMyMongoDatabaseSettings
    {
        /// <summary>
        /// 连接字符串
        /// </summary>
        string ConnectionString { get; set; }

        /// <summary>
        /// 数据库名称
        /// </summary>
        string DatabaseName { get; set; }
    }
}
