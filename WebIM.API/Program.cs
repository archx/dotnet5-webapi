using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using Autofac.Configuration;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration.Json;

namespace WebIM.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args)
                .ConfigureLogging(builder =>
                {
                    builder.AddLog4Net("Config/log4net.config");
                })
                .UseServiceProviderFactory(new AutofacServiceProviderFactory(builder =>
                {
                    // 配置文件注册
                    IConfigurationBuilder config = new ConfigurationBuilder();
                    IConfigurationSource source = new JsonConfigurationSource()
                    {
                        Path = "Config/autofac.json",
                        Optional = false,
                        ReloadOnChange = true,
                    };
                    config.Add(source);
                    ConfigurationModule module = new ConfigurationModule(config.Build());

                    builder.RegisterModule(module);
                }))
                .Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
