﻿using System.Net.Http;

namespace WebIM.API.Services
{
    
    /// <summary>
    /// 环信接口实现
    /// </summary>
    public class EasemobService : IEasemobService
    {
        private readonly HttpClient _client;

        public EasemobService(IHttpClientFactory factory)
        {
            _client = factory.CreateClient();
        }
    }

    /// <summary>
    /// 环信API接口
    /// </summary>
    public interface IEasemobService
    {
        
    }
}