﻿using System;

namespace WebIM.API.Common.Exceptions
{
    /// <summary>
    /// 消息状态异常
    /// </summary>
    public class MessageCodeException : Exception, IMessageCode
    {
        /// <inheritdoc />
        public int Code { get; }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="code">状态码</param>
        /// <param name="message">消息</param>
        public MessageCodeException(int code, string message) : base(message)
        {
            Code = code;
        }
    }
}