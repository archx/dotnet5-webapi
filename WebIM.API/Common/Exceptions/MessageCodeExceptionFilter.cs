﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace WebIM.API.Common.Exceptions
{
    /// <summary>
    /// 消息状态异常拦截器
    /// </summary>
    public class MessageCodeExceptionFilter : IActionFilter, IOrderedFilter
    {
        /// <inheritdoc />
        public void OnActionExecuting(ActionExecutingContext context)
        {
        }

        /// <inheritdoc />
        public void OnActionExecuted(ActionExecutedContext context)
        {
            if (context.Exception is MessageCodeException exception)
            {
                context.Result = new JsonResult(((IMessageCode) exception).ConvertTo());
                context.ExceptionHandled = true;
            }
        }

        /// <inheritdoc />
        public int Order { get; } = int.MaxValue - 10;
    }
}