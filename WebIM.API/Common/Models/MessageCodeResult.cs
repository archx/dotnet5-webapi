﻿using System.Text.Json.Serialization;
using WebIM.API.Utilities;

namespace WebIM.API.Common.Models
{
    /// <summary>
    /// 接口响应体
    /// </summary>
    /// <typeparam name="T">数据类型</typeparam>
    public class MessageCodeResult<T>
    {
        /// <summary>
        /// 状态码
        /// </summary>

        [JsonPropertyName("errcode")]
        public int Code { set; get; }

        /// <summary>
        /// 消息
        /// </summary>
        [JsonPropertyName("errmsg")]
        public string Message { set; get; }

        /// <summary>
        /// 数据内容
        /// </summary>
        public T Result { set; get; }

        /// <summary>
        /// 服务器时间戳
        /// </summary>
        public long Timestamp { set; get; }

        public MessageCodeResult()
        {
            Code = 0;
            Message = "ok";
            Timestamp = DateUtils.Timestamp();
        }

        private MessageCodeResult(int code, string message, T result = default)
        {
            Code = code;
            Message = message;
            Result = result;
            Timestamp = DateUtils.Timestamp();
        }

        public static MessageCodeResult<T> Ok()
        {
            return new();
        }

        public static MessageCodeResult<T> Ok(T result)
        {
            var resp = new MessageCodeResult<T>
            {
                Result = result
            };
            return resp;
        }

        public static MessageCodeResult<T> Fail()
        {
            var resp = new MessageCodeResult<T>(1, "fail");
            return resp;
        }

        public static MessageCodeResult<T> Fail(string message) => Fail(1, message);

        public static MessageCodeResult<T> Fail(int code, string message)
        {
            var resp = new MessageCodeResult<T>(code, message);
            return resp;
        }
    }
}