﻿using WebIM.API.Common.Models;

namespace WebIM.API.Common
{
    /// <summary>
    /// 消息状态码接口
    /// </summary>
    public interface IMessageCode
    {
        /// <summary>
        /// 状态码
        /// </summary>
        int Code { get; }

        /// <summary>
        /// 消息
        /// </summary>
        string Message { get; }

        /// <summary>
        /// 将消息状态转换为响应Model
        /// </summary>
        /// <returns>MessageCodeResult</returns>
        MessageCodeResult<object> ConvertTo() 
            => MessageCodeResult<object>.Fail(Code, Message);
    }
}