﻿using System;

namespace WebIM.API.Utilities
{
    /// <summary>
    /// 日期辅助类，提供类Java日期处理
    /// </summary>
    public static class DateUtils
    {
        /// <summary>
        /// 获取当前时间戳：毫秒
        /// </summary>
        /// <returns>当前时间戳</returns>
        public static long Timestamp()
        {
            TimeSpan ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return Convert.ToInt64(ts.TotalMilliseconds);
        }

        /// <summary>
        /// 获取当前时间 expiresIn 秒之后的时间戳
        /// </summary>
        /// <param name="seconds">秒</param>
        /// <returns>时间戳</returns>
        public static long TimestampAfterSeconds(int seconds)
        {
            return Timestamp() + seconds * 1000;
        }
    }
}