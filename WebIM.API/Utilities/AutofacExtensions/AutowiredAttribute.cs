﻿using System;
using System.Linq;
using System.Reflection;
using Autofac.Core;

namespace WebIM.API.Utilities.AutofacExtensions
{
    /// <summary>
    /// 类似Spring @Autowired
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class AutowiredAttribute : Attribute
    {
    }

    /// <summary>
    /// @Autowired 属性选择器
    /// </summary>
    public class AutowiredPropertySelector : IPropertySelector
    {
        /// <summary>
        /// 判断属性是否需要注入
        /// </summary>
        /// <param name="propertyInfo">属性信息</param>
        /// <param name="instance">实例</param>
        /// <returns>bool</returns>
        public bool InjectProperty(PropertyInfo propertyInfo, object instance)
        {
            return propertyInfo.CustomAttributes.Any(it => it.AttributeType == typeof(AutowiredAttribute));
        }
    }
}