﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Quartz;
using Quartz.Spi;

namespace WebIM.API.Schedules.Quartz
{
    /// <summary>
    /// 中介者工厂实现
    /// </summary>
    public class RunnerJobFactory : IJobFactory
    {
        private readonly IServiceProvider _serviceProvider;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="serviceProvider">sp</param>
        public RunnerJobFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        /// <summary>
        /// 始终返回 QuartzJobRunner 实例，使用中介者模式执行
        /// </summary>
        /// <param name="bundle"></param>
        /// <param name="scheduler"></param>
        /// <returns></returns>
        public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler)
        {
            return _serviceProvider.GetRequiredService<QuartzJobRunner>();
        }

        /// <summary>
        /// ignore
        /// </summary>
        /// <param name="job"></param>
        public void ReturnJob(IJob job)
        {
            // throw new System.NotImplementedException();
        }
    }
}