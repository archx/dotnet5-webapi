﻿using System;
using System.ComponentModel;

namespace WebIM.API.Schedules.Quartz
{
    /// <summary>
    /// 作业调度
    /// </summary>
    public class JobSchedule
    {
        /// <summary>
        /// 构造任务调度
        /// </summary>
        /// <param name="jobType">Job类型</param>
        /// <param name="cronExpression">cron表达式</param>
        public JobSchedule(Type jobType, string cronExpression)
        {
            JobType = jobType ?? throw new ArgumentNullException(nameof(jobType));
            CronExpression = cronExpression ?? throw new ArgumentNullException(nameof(cronExpression));
        }

        /// <summary>
        /// Job 类型
        /// </summary>
        public Type JobType { get; private set; }

        /// <summary>
        /// Cron 表达式
        /// </summary>
        public string CronExpression { get; private set; }

        /// <summary>
        /// Job 运行状态
        /// </summary>
        public JobStatus JobStatus { get; set; } = JobStatus.Init;
    }

    /// <summary>
    /// 任务状态
    /// </summary>
    public enum JobStatus : byte
    {
        /// <summary>
        /// 初始化
        /// </summary>
        [Description("初始化")] Init = 0,

        /// <summary>
        /// 运行中
        /// </summary>
        [Description("运行中")] Running = 1,

        /// <summary>
        /// 调度中
        /// </summary>
        [Description("调度中")] Scheduling = 2,

        /// <summary>
        /// 已停止
        /// </summary>
        [Description("已停止")] Stopped = 3,
    }
}