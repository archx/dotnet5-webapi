﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Quartz;
using Quartz.Spi;

namespace WebIM.API.Schedules.Quartz
{
    /// <summary>
    /// IHostedService 实现，设置 Quartz 调度程序
    /// </summary>
    public class QuartzHostedService : IHostedService
    {
        private readonly ISchedulerFactory _schedulerFactory;
        private readonly IJobFactory _jobFactory;
        private readonly IEnumerable<JobSchedule> _jobSchedules;

        /// <summary>
        /// 调度器
        /// </summary>
        public IScheduler Scheduler { get; set; }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="schedulerFactory">调度器工厂</param>
        /// <param name="jobFactory">任务工厂类</param>
        /// <param name="jobSchedules">具体作业</param>
        public QuartzHostedService(ISchedulerFactory schedulerFactory, IJobFactory jobFactory,
            IEnumerable<JobSchedule> jobSchedules)
        {
            _schedulerFactory = schedulerFactory;
            _jobFactory = jobFactory;
            _jobSchedules = jobSchedules;
        }

        /// <summary>
        /// 应用程序启动时调用，循环注入作业计划，开始任务调度
        /// </summary>
        /// <param name="cancellationToken">token</param>
        /// <returns></returns>
        public async Task StartAsync(CancellationToken cancellationToken)
        {
            Scheduler = await _schedulerFactory.GetScheduler(cancellationToken);
            Scheduler.JobFactory = _jobFactory;
            foreach (var jobSchedule in _jobSchedules)
            {
                var job = CreateJob(jobSchedule);
                var trigger = CreateTrigger(jobSchedule);
                await Scheduler.ScheduleJob(job, trigger, cancellationToken);
                jobSchedule.JobStatus = JobStatus.Scheduling;
            }

            await Scheduler.Start(cancellationToken);
            foreach (var jobSchedule in _jobSchedules)
            {
                jobSchedule.JobStatus = JobStatus.Running;
            }
        }

        /// <summary>
        /// 应用程序结束时，结束任务调度
        /// </summary>
        /// <param name="cancellationToken">token</param>
        /// <returns></returns>
        public async Task StopAsync(CancellationToken cancellationToken)
        {
            if (Scheduler != null)
            {
                await Scheduler.Shutdown(cancellationToken);
            }

            foreach (var jobSchedule in _jobSchedules)
            {
                jobSchedule.JobStatus = JobStatus.Stopped;
            }
        }

        private static IJobDetail CreateJob(JobSchedule schedule)
        {
            var jobType = schedule.JobType;
            return JobBuilder
                .Create(jobType)
                .WithIdentity(jobType.FullName ?? jobType.Name)
                .WithDescription(jobType.Name)
                .Build();
        }

        private static ITrigger CreateTrigger(JobSchedule schedule)
        {
            return TriggerBuilder
                .Create()
                .WithIdentity($"{schedule.JobType.FullName}.trigger")
                .WithCronSchedule(schedule.CronExpression)
                .WithDescription(schedule.CronExpression)
                .Build();
        }
    }
}