﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebIM.API.Controllers
{
    /// <summary>
    /// 聊天历史记录接口
    /// </summary>
    [ApiController]
    [Route("chat/history")]
    public class ChatHistoryController : ControllerBase
    {
    }
}
